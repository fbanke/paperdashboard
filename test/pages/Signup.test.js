import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Signup from '../../src/pages/Signup.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Signup', () => {
  let actions
  let store
  let getters
  let mocks

  beforeEach(() => {
    mocks = {
      $t: (msg) => { return msg }
    }
    actions = {
      userSignUp: jest.fn(),
    }
    getters = {
      error: jest.fn()
    }

    store = new Vuex.Store({
      state: { error: undefined, loading: false },
      actions,
      getters
    })
  })

  it('sets the correct default data', () => {
    expect(typeof Signup.data).toBe('function')
    const defaultData = Signup.data()

    expect(defaultData.email).toBe('')
    expect(defaultData.password).toBe('')
  })

  it('triggers userSignUp action on signup button click with data', () => {
    const wrapper = shallowMount(Signup, { localVue, store, mocks, stubs: ['router-link'] })

    wrapper.setData({ email: 'e@mail.com' })
    wrapper.setData({ password: 'password' })
    wrapper.find('#Signup').trigger('submit')
    
    expect(actions.userSignUp.mock.calls).toHaveLength(1)
    expect(actions.userSignUp.mock.calls[0][1]).toEqual({email: 'e@mail.com', password: 'password'})
  })

  it('unknown error shows default error message', () => {
    getters.error.mockReturnValueOnce('random error')
    const wrapper = shallowMount(Signup, { localVue, store, mocks, stubs: ['router-link'] })

    expect(wrapper.find('#error').text()).toBe("UNKNOWN_ERROR")
  })

})
