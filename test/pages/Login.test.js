import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Login from '../../src/pages/Login.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Login', () => {
  let actions
  let store
  let getters
  let mocks

  beforeEach(() => {
    mocks = {
      $t: (msg) => { return msg }
    }
    actions = {
      userSignIn: jest.fn(),
      userResendVerification: jest.fn()
    }
    getters = {
      error: jest.fn()
    }

    store = new Vuex.Store({
      state: { error: undefined, loading: false },
      actions,
      getters
    })
  })

  it('sets the correct default data', () => {
    expect(typeof Login.data).toBe('function')
    const defaultData = Login.data()

    expect(defaultData.email).toBe('')
    expect(defaultData.password).toBe('')
  })

  it('triggers userSignIn action on login button click with data', () => {
    const wrapper = shallowMount(Login, { localVue, store, mocks, stubs: ['router-link'] })

    wrapper.setData({ email: 'e@mail.com' })
    wrapper.setData({ password: 'password' })
    wrapper.find('#Login').trigger('submit')
    
    expect(actions.userSignIn.mock.calls).toHaveLength(1)
    expect(actions.userSignIn.mock.calls[0][1]).toEqual({email: 'e@mail.com', password: 'password'})
  })

  it('unknown error shows default error message', () => {
    getters.error.mockReturnValueOnce('random error')
    const wrapper = shallowMount(Login, { localVue, store, mocks, stubs: ['router-link'] })

    expect(wrapper.find('#error').text()).toBe("UNKNOWN_ERROR")
  })

  it('resend validation error shows send link', () => {
    getters.error.mockReturnValueOnce('AUTH_EMAIL_NOT_VERIFIED')

    const wrapper = shallowMount(Login, { localVue, store, mocks, stubs: ['router-link'] })

    expect(wrapper.find('#error').text()).toBe("AUTH_EMAIL_NOT_VERIFIED")
    expect(wrapper.find('#resendValidationEmail').isVisible()).toBeTruthy()
  })

  it('click resend validation link triggers dispatch', () => {
    getters.error.mockReturnValueOnce('AUTH_EMAIL_NOT_VERIFIED')

    const wrapper = shallowMount(Login, { localVue, store, mocks, stubs: ['router-link'] })
    wrapper.setData({ email: 'e@mail.com' })

    wrapper.find('#resendValidationEmail').trigger('click')

    expect(actions.userResendVerification.mock.calls).toHaveLength(1)
    expect(actions.userResendVerification.mock.calls[0][1]).toEqual({email: 'e@mail.com'})
  })
})
