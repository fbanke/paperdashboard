import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import ForgotPassword from '../../src/pages/ForgotPassword.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Login', () => {
  let actions
  let store
  let getters
  let mocks

  beforeEach(() => {
    mocks = {
      $t: (msg) => { return msg }
    }
    actions = {
      forgotPassword: jest.fn()
    }
    getters = {
      error: jest.fn()
    }

    store = new Vuex.Store({
      state: { error: undefined, loading: false },
      actions,
      getters
    })
  })

  it('sets the correct default data', () => {
    expect(typeof ForgotPassword.data).toBe('function')
    const defaultData = ForgotPassword.data()

    expect(defaultData.email).toBe('')
  })

  it('triggers forgotPassword action on submit button click with data', () => {
    const wrapper = shallowMount(ForgotPassword, { localVue, store, mocks, stubs: ['router-link'] })

    wrapper.setData({ email: 'e@mail.com' })
    wrapper.find('#ForgotPassword').trigger('submit')
    
    expect(actions.forgotPassword.mock.calls).toHaveLength(1)
    expect(actions.forgotPassword.mock.calls[0][1]).toEqual({email: 'e@mail.com'})
  })
})
