import { shallowMount, createLocalVue } from '@vue/test-utils'
import NotLoggedin from '../../src/layout/NotLoggedin.vue'

const localVue = createLocalVue()

describe('NotLoggedin', () => {

  it('correctly sets the body class', () => {
    shallowMount(NotLoggedin, { localVue, stubs: ['router-view'] })

    expect(document.body.classList[0]).toBe('loginForm')
  })

  it('correctly removes the body class', () => {
    const wrapper = shallowMount(NotLoggedin, { localVue, stubs: ['router-view'] })
    wrapper.vm.$destroy()
    
    expect(document.body.classList[0]).toBeFalsy()
  })
})
