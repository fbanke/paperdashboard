import Vue from "vue";
import App from "./App";
import router from "./router/index";
import store from "./store/index";
import axios from "axios";
import firebase from 'firebase'
import VueI18n from 'vue-i18n'

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";

// https://kazupon.github.io/vue-i18n/guide/started.html#html
const messages = {
  en: {
    global: {
      projectname: 'Fitnator'
    }
  },
  da: {
    global: {
      projectname: 'Fitnator'
    }
  }
}

Vue.use(PaperDashboard)
Vue.use(VueI18n)

// Initialize Firebase
var config = {
  apiKey: process.env.VUE_APP_FIREBASE_APIKEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTHDOMAIN,
  databaseURL: process.env.VUE_APP_FIREBASE_DATABASEURL,
  projectId: process.env.VUE_APP_FIREBASE_PROJECTID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGEBUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_SENDERID
};

axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL

firebase.initializeApp(config);

const i18n = new VueI18n({
  locale: 'da', // set locale
  messages, // set locale messages
  silentTranslationWarn: true
})


/* eslint-disable no-new */
// https://medium.com/@oleg.agapov/basic-single-page-application-using-vue-js-and-firebase-part-2-143a3084266f
// https://stackoverflow.com/questions/45290003/router-beforeeach-guard-executed-before-state-loaded-in-vue-created
// onAuthStateChanged is a async function so we must create the vue instance after auth is detected, 
// and remove the listener afterwards
const unsubscribe = firebase.auth().onAuthStateChanged((firebaseUser) => {
  //firebaseUser.auth().languageCode = 'da';
  if (firebaseUser) {
    store.dispatch('autoSignIn', firebaseUser)
  }

  new Vue({
    i18n,
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
  unsubscribe()
})
