import axios from 'axios'

export const API_CONNECT_ERROR = 'API_CONNECT_ERROR'


export function getAPIUserData(token, userID){
    let axios = getAxiosSetup(token)
    return axios.get('/api/User/'+userID)
}

export function updateAPIUserData(token, userID, payload){
    let axios = getAxiosSetup(token)
    return axios.put('/api/User/'+userID, payload)
}

export function getAPIBodyMeasureLatest(token, userID){
    let axios = getAxiosSetup(token)
    return axios.get('/api/BodyMeasure/GetLatest/'+userID)
}

function getAxiosSetup(token){

    let config = createConfig(token)

    const instance = axios.create({
        timeout: 1000,
        headers: config.headers
    });

    return instance;
}

function createConfig(token){
    return {
        headers: {'Authorization': "Bearer " + token, 'Content-Type': 'application/json' }
    }
}