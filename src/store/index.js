import Vue from 'vue'
import Vuex from 'vuex'
import moment from 'moment';
import firebase from 'firebase'
import router from "../router";


import { AUTH_EMAIL_NOT_VERIFIED, AUTH_EMAIL_EXISTS, AUTH_WEAK_PASSWORD, AUTH_LOGIN_FAILED, 
    AUTH_USER_NOT_FOUND, AUTH_PASSWORD_RESET_MAIL_SENT,AUTH_EMAIL_VERIFICATION_SENT, AUTH_EMAIL_VERIFICATION_SEND_FAILED } from '../store/auth_errors.js'
import { API_CONNECT_ERROR, getAPIUserData, updateAPIUserData, getAPIBodyMeasureLatest } from '../store/api.js'


Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    state: {
        user: null,
        error: null,
        message: null,
        loading: false
    },
    actions: {
        getUserWeightData({state}){
            return getAPIBodyMeasureLatest(state.user.token, state.user.data.userID).then((axios) => {
                let labels = []
                let series = []
                
                axios.data.forEach(function (item) {
                    labels.push(moment(item.created).format("D/MM/YY"))
                    series.push(item.weight)
                })

                return {labels, series: [series]}
            })
        },
        forgotPassword({commit}, payload){
            firebase.auth().sendPasswordResetEmail(payload.email)
            .then(() => {
                commit('setMessage', AUTH_PASSWORD_RESET_MAIL_SENT);
            })
            .catch(error => {
                if(error.code == 'auth/user-not-found'){
                    commit('setError', AUTH_USER_NOT_FOUND)
                }else{
                    commit('setError', error.message)
                }
                commit('setLoading', false)
            })
        },
        updateUser({commit, state}, payload){
            commit('setLoading', true) 

            return updateAPIUserData(state.user.token, state.user.userID, {
                firstName: payload.firstName,
                lastName: payload.lastName,
            })
            .then(response => {       
              commit('setUserData', response.data) 
              commit('setLoading', false) 
            })
            .catch(error => { 
                console.log(error)
                commit('setError', API_CONNECT_ERROR)
                commit('setLoading', false) 
            })

        },
        fetchApiUserData ({commit, state}) {  
            commit('setLoading', true) 
            return getAPIUserData(state.user.token, state.user.userID)
                .then(response => {       
                    commit('setUserData', response.data) 
                    commit('setLoading', false) 
                })
                .catch(error => { 
                    console.error(error)
                    commit('setError', API_CONNECT_ERROR)
                    commit('setLoading', false) 
                })
        },
        autoSignIn ({dispatch, commit}, payload) {
            return payload.getIdToken().then(token => {
                commit('setUserContext', {
                    token: token,
                    email: payload.email,
                    userID: payload.uid,
                    emailVerified: payload.emailVerified
                })
                dispatch("fetchApiUserData").then( x => {
                    router.push('/dashboard')
                })
            })
        },
        userSignUp ({commit, dispatch}, payload) {
            commit('setLoading', true)
            
            return firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(firebaseUser => {
              firebaseUser.user.getIdToken().then(token => {
                commit('setUserContext', {
                    token: token,
                    email: payload.email,
                    userID: payload.uid,
                    emailVerified: false
                })

                // send verification email
                dispatch('userResendVerification').then(() => {
                    commit('setLoading', false)
                    router.push('/unauth/signup-next-step')
                    commit('setError', null)
                })
             })
            })
            .catch(error => {
                if(error.code == 'auth/email-already-in-use'){
                    commit('setError', AUTH_EMAIL_EXISTS)
                }else if(error.code == 'auth/weak-password'){
                    commit('setError', AUTH_WEAK_PASSWORD)
                }else{
                    commit('setError', error.message)
                }
                commit('setLoading', false)
            })
        },
        userSignIn ({dispatch, commit}, payload) {
            commit('setLoading', true)
            
            return firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            .then(firebaseUser => {
                if( ! firebaseUser.user.emailVerified){
                    commit('setError', AUTH_EMAIL_NOT_VERIFIED)
                    return;
                }
                
                return firebaseUser.user.getIdToken().then(token => {
                    commit('setUserContext', {
                        token: token,
                        email: payload.email,
                        userID: firebaseUser.user.uid,
                        emailVerified: firebaseUser.user.emailVerified
                    })
                    dispatch("fetchApiUserData").then(x => {
                        commit('setLoading', false)
                        commit('setError', null)
                    })

                    router.push("/dashboard")
                })
            })
            .catch(error => {
                if(error.code == 'auth/wrong-password' || error.code == 'auth/user-not-found'){
                    commit('setError', AUTH_LOGIN_FAILED)
                }else{
                    commit('setError', error.message)
                }
                commit('setLoading', false)
            })
        },
        userSignOut({commit}) {
            firebase.auth().signOut().then(function() {
                // Sign-out successful.
                commit('resetUser')
                router.push('/')
            }, function() {
                // An error happened.
                commit('resetUser')
                router.push('/')
            });
        },
        userResendVerification({commit}){
            let user = firebase.auth().currentUser
            
            return user.sendEmailVerification().then(function() {
                commit('setMessage', AUTH_EMAIL_VERIFICATION_SENT);
            }).catch(function(error) {
                commit('setError', AUTH_EMAIL_VERIFICATION_SEND_FAILED);
            })
        }
    },
    mutations: {
        setUserContext(state, payload){
            if( ! state.user){
                state.user = {}
            }
            state.user.email = payload.email
            state.user.emailVerified = payload.emailVerified
            state.user.token = payload.token
            state.user.userID = payload.userID
        },
        setUserData(state, payload){
            if( ! state.user){
                state.user = {}
            }
            state.user.data = payload;
        },
        resetUser(state){
            state.user = undefined
        },
        setError (state, payload) {
            state.error = payload
        },
        setMessage (state, payload) {
            state.message = payload
        },
        setLoading (state, payload) {
            state.loading = payload
        }
    },
    getters: {
        loggedIn: (state) => () => {
            if(state.user){
                return state.user.emailVerified
            }
            return false
        },
        error: state => {
            return state.error
        },
        message: state => {
            return state.message
        },
        loading: state => {
            return state.loading
        }
    },

    strict: debug
})