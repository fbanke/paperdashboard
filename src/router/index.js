import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import store from "../store";

Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "active"
});

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters.loggedIn()
  
  const record = to.matched.find(record => record.meta.loggedInRedirect) // find the first matching route

  if (record) {
    
    if (isAuthenticated) {
      next({
        path: record.meta.loggedInRedirect.loggedIn
      })
      return
    }else{
      next({
        path: record.meta.loggedInRedirect.notLoggedIn
      })
    }
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    //const isAuthenticated = firebase.auth().currentUser
    if (!isAuthenticated) {
      next({
        path: '/unauth/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router;
