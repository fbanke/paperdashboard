import DashboardLayout from "@/layout/dashboard/DashboardLayout.vue";
import NotLoggedinLayout from "@/layout/NotLoggedin.vue";
// GeneralViews
import NotFound from "@/pages/NotFoundPage.vue";

// Admin pages
import Dashboard from "@/pages/Dashboard.vue";
import UserProfile from "@/pages/UserProfile.vue";
import Notifications from "@/pages/Notifications.vue";
import FitnessShape from "@/pages/FitnessShape.vue";
import Food from "@/pages/Food.vue";
import Training from "@/pages/Training.vue";
import TableList from "@/pages/TableList.vue";
import Login from "@/pages/Login.vue";
import SignupNextStep from "@/pages/SignupNextStep.vue";
import Signup from "@/pages/Signup.vue";
import ForgotPassword from "@/pages/ForgotPassword.vue";

const routes = [
  {
    path: "/",
    meta: { 
      loggedInRedirect: {
        loggedIn: '/dashboard',
        notLoggedIn: '/unauth/login'
      }
    },
  },
  {
    path: "/unauth",
    component: NotLoggedinLayout,
    children: [
      {
        path: "login",
        name: "login",
        component: Login,
      },
      {
        path: "signup",
        name: "signup",
        component: Signup,
      },
      {
        path: "signup-next-step",
        name: "signup-next-step",
        component: SignupNextStep,
      },
      {
        path: "forgot-password",
        name: "forgot-password",
        component: ForgotPassword,
      }
    ]
  },
  {
    path: "/",
    component: DashboardLayout,
    meta: { requiresAuth: true },
    children: [
      {
        path: "dashboard",
        name: "Oversigt",
        component: Dashboard,
      },
      {
        path: "stats",
        name: "stats",
        component: UserProfile
      },
      {
        path: "notifications",
        name: "notifications",
        component: Notifications
      },
      {
        path: "shape",
        name: "Form",
        component: FitnessShape
      },
      {
        path: "food",
        name: "Kost",
        component: Food
      },
      {
        path: "training",
        name: "Træning",
        component: Training
      },
      {
        path: "table-list",
        name: "table-list",
        component: TableList
      }
    ]
  },
  { path: "*", component: NotFound }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
